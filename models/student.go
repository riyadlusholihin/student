package models

type Student struct {
	ID    int    `json:"id"`
	Name  string `name:"name"`
	Email string `json:"email"`
}
