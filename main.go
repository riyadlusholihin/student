package main

import (
	"fmt"
	"log"
	"net/http"
	"student/router"
)

func main() {

	//show semua data dan search
	http.HandleFunc("/student", router.GetStudent)
	//untuk add data
	http.HandleFunc("/add", router.AddStudent)
	//untuk update
	http.HandleFunc("/update-student", router.UpdateStudent)

	fmt.Println("Http server http://localhost:8081")
	err := http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatal(err)
	}
}
