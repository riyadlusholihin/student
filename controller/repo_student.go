package controller

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"student/config"
	"student/models"
)

const (
	table = "student"
)

func GetAll(ctx context.Context, r *http.Request) ([]models.Student, error) {
	var students []models.Student
	db, err := config.MySQL()
	qemail := r.URL.Query().Get("search")

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}
	// query := fmt.Sprintf("SELECT * FROM student Order By id DESC")
	query := fmt.Sprintf("SELECT * FROM student WHERE email LIKE '%s' OR id LIKE '%s' Order By id DESC", "%"+qemail+"%", "%"+qemail+"%")

	rowQuery, err := db.QueryContext(ctx, query)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var stn models.Student
		if err = rowQuery.Scan(&stn.ID,
			&stn.Name,
			&stn.Email); err != nil {
			return nil, err
		}
		students = append(students, stn)
	}
	defer db.Close()
	return students, nil
}

func Insert(ctx context.Context, stn models.Student) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Not connect to MYSQL", err)
	}
	var results models.Student
	err = db.QueryRow("SELECT email FROM student WHERE email=?", stn.Email).Scan(&results.Email)

	if len(results.Email) > 0 {
		return errors.New("Email Sudah Digunakan")
	} else {
		query := fmt.Sprintf("INSERT INTO %v (name,email) values('%v','%v')", table,
			stn.Name, stn.Email)
		_, err = db.ExecContext(ctx, query)
		if err != nil {
			return err
		}
		defer db.Close()
	}
	return nil
}

func Update(ctx context.Context, stn models.Student, r *http.Request) error {
	db, err := config.MySQL()
	nid := r.URL.Query().Get("id")
	if err != nil {
		log.Fatal("Can't connect to mysql", err)
	}

	queryText := fmt.Sprintf("UPDATE %v SET name = '%s', email = '%s' WHERE id = %s", table, stn.Name, stn.Email, nid)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	defer db.Close()
	return nil
}

// func GetOne(ctx context.Context, r *http.Request) ([]models.Student, error) {
// 	var students []models.Student
// 	db, err := config.MySQL()
// 	qemail := r.URL.Query().Get("search")

// 	if err != nil {
// 		log.Fatal("Cant connect to MySQL", err)
// 	}

// 	query := fmt.Sprintf("SELECT * FROM student WHERE email LIKE '%s' OR id LIKE '%s'", "%"+qemail+"%", "%"+qemail+"%")

// 	rowQuery, err := db.QueryContext(ctx, query)

// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	for rowQuery.Next() {
// 		var stn models.Student
// 		if err = rowQuery.Scan(&stn.ID,
// 			&stn.Name,
// 			&stn.Email); err != nil {
// 			return nil, err
// 		}
// 		students = append(students, stn)
// 	}
// 	defer db.Close()
// 	return students, nil
// }
