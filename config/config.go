package config

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

const (
	username string = "root"
	password string = ""
	database string = "klik_test"
)

var (
	dsn = fmt.Sprintf("%v:%v@tcp(127.0.0.1:3306)/%v", username, password, database)
)

func MySQL() (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)

	if err != nil {
		return nil, err
	}

	return db, nil
}
