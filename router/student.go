package router

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"student/controller"
	"student/models"
	"student/untils"
)

//getStudent
func GetStudent(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		students, err := controller.GetAll(ctx, r)

		if err != nil {
			fmt.Println(err)
		}
		untils.ResponseJSON(w, students, http.StatusOK)
		return
	}
	http.Error(w, "not found", http.StatusNotFound)
	return
}

func AddStudent(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "Hanya Menerima applicaiton/json", http.StatusBadRequest)
			return
		}

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var stn models.Student

		if err := json.NewDecoder(r.Body).Decode(&stn); err != nil {
			untils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}

		if err := controller.Insert(ctx, stn); err != nil {
			res := map[string]string{
				"Message": "Email Sudah Digunakan",
			}
			untils.ResponseJSON(w, res, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"Message": "succes add student",
		}
		untils.ResponseJSON(w, res, http.StatusCreated)
		return
	}
	http.Error(w, "method tidak match", http.StatusMethodNotAllowed)
	return
}

func UpdateStudent(w http.ResponseWriter, r *http.Request) {
	if r.Method == "PUT" {
		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "Hanya Menerima applicaiton/json", http.StatusBadRequest)
			return
		}

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var stn models.Student

		if err := json.NewDecoder(r.Body).Decode(&stn); err != nil {
			untils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}

		if err := controller.Update(ctx, stn, r); err != nil {
			untils.ResponseJSON(w, err, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"Message": "Succes Update Data",
		}

		untils.ResponseJSON(w, res, http.StatusCreated)
		return
	}
	http.Error(w, "method not match", http.StatusMethodNotAllowed)
	return
}

// func GetOnly(w http.ResponseWriter, r *http.Request) {
// 	if r.Method == "GET" {

// 		ctx, cancel := context.WithCancel(context.Background())
// 		defer cancel()

// 		students, err := controller.GetOne(ctx, r)

// 		if err != nil {
// 			fmt.Println(err)
// 		}
// 		untils.ResponseJSON(w, students, http.StatusOK)
// 		return
// 	}
// 	http.Error(w, "method tidak match", http.StatusMethodNotAllowed)
// 	return
// }
